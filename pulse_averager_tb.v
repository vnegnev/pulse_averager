`timescale 1ns/1ns

module pulse_averager(
		      input 	    clk,
		      input 	    pedge,
		      output reg [15:0] rate);

   parameter k = 8;
   parameter m = 11;

   initial begin
      rate = 0; // can't do this for CPLDs, but doesn't matter because it'll decay very quickly
   end
   
   always @(posedge clk) begin      
      rate <= rate + (pedge << (k-m)) - (rate >> m);
   end

endmodule // pulse_averager


module pulse_averager_tb;
   parameter k = 16; // freq gain
   parameter m = 10; // time constant
   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   wire [15:0]		rate;			// From UUT of pulse_averager.v
   // End of automatics
   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   reg			clk;			// To UUT of pulse_averager.v
   reg			pedge;			// To UUT of pulse_averager.v
   // End of automatics

   integer 		pedge_t = 2;

   pulse_averager #(/*AUTOINSTPARAM*/
		    // Parameters
		    .k			(k),
		    .m			(m))
   UUT(/*AUTOINST*/
       // Outputs
       .rate				(rate[15:0]),
       // Inputs
       .clk				(clk),
       .pedge				(pedge));

   initial begin
      $dumpfile("icarus_compile/000_pulse_averager_tb.lxt");
      $dumpvars(0, pulse_averager_tb);
      
      clk = 0;
      pedge = 0;

      #100000 pedge_t = 3;
      #100000 pedge_t = 5;
      #100000 pedge_t = 100;            
      
      #100000 $finish;
   end

   // Clock generation
   always #5 clk = !clk;
   always begin
      #10 pedge = 1;
      #10 pedge = 0;
      #(10*(pedge_t-2));
   end

endmodule // pulse_averager_tb

